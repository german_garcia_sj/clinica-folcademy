package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnosController {

    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @GetMapping(value = "")
    public ResponseEntity<List<Turno>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos())
                ;
    }
}
